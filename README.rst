.. GeneFlow Example Workflows

=====================
Test Workflow for GF2
=====================

Version: v0.3

This is an example GeneFlow bioinformatics workflow with two steps: 1) Reference index creation with BWA Index, and 2) Sequence alignment with BWA Mem.

